<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

A small scripting language to embed in your flutter projects.

This started out as an effort to add some simple maths, loaded from a database, to a personal
project. I planned a miniature scripting interpreter - named "horriblescript" because it was going
to be a quick and dirty implementation.

On realising it was far less hacky than anticipated, I've cleaned it up for release.

## Features

- Lightweight scripting. Define functions. create loops. Assign variables. All the usuals, none of
  the extras.
- Call dart functions from flight, or call flight from dart.
- Reverse polish syntax
- VM is language agnostic - implement a parser and to implement a new language on the Flight VM

## Getting started

Initializing the interpreter and running a script:

```dart
import 'package:flight_script/flight_script.dart';

void main(List<String> args) {
  final interpreter = Interpreter();
  final result = interpreter.eval('"Hello, World!"');

  print(result);
}
```

Result

```
[Hello, World!]
```

Notice that returned values are returned in an array.

## Usage

Take the first n numbers passed by dart:

```dart
import 'package:flight_script/flight_script.dart';

void main(List<String> args) {
  // repeat the passed string n times
  // loop is a standard library function, that calls the provided block with an index (i)
  // from the start index to the end index.
  // 
  final repeats = """
      { :i
        input
      } 1 n loop()
    """;
  final interpreter = Interpreter();
  interpreter.bind("n", 5);
  interpreter.bind("input", "Hello");
  final result = interpreter.eval(repeats);
  print(result);
}
```

Result

```
[Hello, Hello, Hello, Hello, Hello]
```

Call a dart function from flightscript

```dart
import 'package:flight_script/flight_script.dart';

/// A virtual stack is passed to the function
/// arguments are on the stack, results should be pushed to it
/// much the same idea as Lua's C API
void add(IMachine state) {
  final a = state.popNumber();
  final b = state.popNumber();
  state.pushNumber(a + b);
}

void main(List<String> args) {
  final interpreter = Interpreter();
  interpreter.bind("add", add);
  final result = interpreter.eval("3 4 add()");
  print(result);
}
```

TODO: Include short and useful examples for package users. Add longer examples to `/example` folder.

## Additional information

None...
