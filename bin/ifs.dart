import 'package:flight_script/flight_script.dart';

const first5Fib = """
  {
    dup()
    1 >()
    {
       dup() 1 -() fib() +()
    }
    if()
  } :fib
  { fib() } 1 5 loop()

""";


const arrayTest = """
  []()
  "Hello"
  "World"
  mapSet()
  inspect()
""";

/// traditional style functions - stop using the stack
const traditionalTest = """

  { :input
    1 input +() print()
  } :print_plus_one
  
  5 print_plus_one()

""";

void add(IMachine state){
  final a = state.popNumber();
  final b = state.popNumber();
  state.pushNumber(a + b);
}

void main(List<String> args) {
  final interpreter = Interpreter();
  interpreter.bind("add", add);
  final result = interpreter.eval("3 4 add()");
  print(result);
}
