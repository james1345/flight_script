enum CommandType {
  Push,
  PushBool,
  PushNumber,
  PushString,
  PushDartFunction,
  PushFunction,
  PushMap,
  PushObject,
  Pop,
  Store,
  Load,
  Execute,
}

abstract class Command<T> {
  final CommandType type;
  final T data;

  const Command(this.type, this.data);
}
