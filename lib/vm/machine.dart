import 'package:flight_script/vm/command_base.dart';
import 'package:flight_script/vm/value_base.dart';
import 'package:flight_script/vm/value_implementations.dart';
import 'package:flight_script/vm/i_machine.dart';

class Machine implements IMachine {
  final List<FlightValue> _stack = [];
  final List<Map<FlightValue, FlightValue>> _frames = [{}];

  @override
  dynamic call(Command command) {
    switch (command.type) {
      case CommandType.Push:
        return _push(command.data);
      case CommandType.PushBool:
        return _push(BoolValue(command.data));
      case CommandType.PushNumber:
        return _push(NumberValue(command.data));
      case CommandType.PushString:
        return _push(StringValue(command.data));
      case CommandType.PushDartFunction:
        return _push(DartFunctionValue(command.data));
      case CommandType.PushFunction:
        return _push(FlightFunctionValue(command.data));
      case CommandType.PushMap:
        return _push(MapValue());
      case CommandType.PushObject:
        return _push(DartObjectValue(command.data));
      case CommandType.Pop:
        return _pop();
      case CommandType.Store:
        return _store();
      case CommandType.Load:
        return _load();
      case CommandType.Execute:
        return _execute();
    }
  }

  void _execute() {
    _frames.add({});

    final fn = _pop();
    if (fn.type == FlightValueType.DartFunction) {
      fn.value(this);
    } else if (fn.type == FlightValueType.Function) {
      for (final command in fn.value.commands) {
        call(command);
      }
    } else {
      throw TypeError();
    }

    _frames.removeLast();
  }

  void _load() {
    final key = _pop();
    final value = _frames.lastWhere((it) => it.keys.contains(key))[key];
    _push(value!);
  }

  FlightValue _pop() => _stack.removeLast();

  void _push(FlightValue value) => _stack.add(value);

  void _store() {
    final key = _pop();
    final value = _pop();
    final frame = _frames.last;
    frame[key] = value;
  }
}
