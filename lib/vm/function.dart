import 'command_base.dart';

class FlightFunction {
  final Iterable<Command> commands;

  const FlightFunction(this.commands);
}
