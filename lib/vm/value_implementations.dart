import 'package:flight_script/vm/function.dart';
import 'package:flight_script/vm/i_machine.dart';
import 'package:flight_script/vm/value_base.dart';

abstract class CallableFlightValue {
  void call(IMachine state);
}

class BoolValue extends FlightValue<bool> {
  const BoolValue(bool value) : super(FlightValueType.Bool, value);
}

class NumberValue extends FlightValue<num> {
  const NumberValue(num value) : super(FlightValueType.Number, value);
}

class StringValue extends FlightValue<String> {
  const StringValue(String value) : super(FlightValueType.String, value);
}

class FlightFunctionValue extends FlightValue<FlightFunction> {
  const FlightFunctionValue(FlightFunction value)
      : super(FlightValueType.Function, value);
}

class DartFunctionValue extends FlightValue<DartFunction> {
  const DartFunctionValue(DartFunction value)
      : super(FlightValueType.DartFunction, value);
}

/// Map values are not immutable, unlike other values
class MapValue extends FlightValue<Map<FlightValue, FlightValue>> {
  MapValue() : super(FlightValueType.Map, {});

}

class DartObjectValue extends FlightValue<dynamic> {
  DartObjectValue(value) : super(FlightValueType.DartObject, value);
}
