import 'package:flight_script/vm/command_base.dart';
import 'package:flight_script/vm/function.dart';
import 'package:flight_script/vm/i_machine.dart';
import 'package:flight_script/vm/value_base.dart';

class Push extends Command<FlightValue> {
  const Push(FlightValue data) : super(CommandType.Push, data);
}

class PushBool extends Command<bool> {
  const PushBool(bool data) : super(CommandType.PushBool, data);
}

class PushNumber extends Command<double> {
  const PushNumber(double data) : super(CommandType.PushNumber, data);
}

class PushString extends Command<String> {
  const PushString(String data) : super(CommandType.PushString, data);
}

class PushDartFunction extends Command<DartFunction> {
  const PushDartFunction(DartFunction data)
      : super(CommandType.PushDartFunction, data);
}

class PushFunction extends Command<FlightFunction> {
  const PushFunction(FlightFunction data)
      : super(CommandType.PushFunction, data);
}

class PushMap extends Command<void> {
  const PushMap() : super(CommandType.PushMap, null);
}

class PushObject extends Command<dynamic> {
  const PushObject(dynamic data) : super(CommandType.PushObject, data);
}

class Pop extends Command<void> {
  const Pop() : super(CommandType.Pop, null);
}

class Store extends Command<void> {
  const Store() : super(CommandType.Store, null);
}

class Load extends Command<void> {
  const Load() : super(CommandType.Load, null);
}

class Execute extends Command<void> {
  const Execute() : super(CommandType.Execute, null);
}
