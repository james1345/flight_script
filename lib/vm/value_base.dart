enum FlightValueType {
  Bool,
  Number,
  String,
  DartFunction,
  Function,
  Map,
  DartObject,
}

extension on FlightValueType {
  String get name => toString().split(".").last;
}

abstract class FlightValue<T> {
  final FlightValueType type;
  final T value;

  const FlightValue(this.type, this.value);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FlightValue &&
          runtimeType == other.runtimeType &&
          type == other.type &&
          value == other.value;

  @override
  int get hashCode => type.hashCode ^ value.hashCode;

  @override
  String toString() {
    return '${type.name.toLowerCase()},$value';
  }
}
