
import 'command_base.dart';

abstract class IMachine {

  dynamic call(Command command);

}

typedef DartFunction = void Function(IMachine);