library flight_script;

import 'rpn/lexer.dart';
import 'rpn/parser.dart';
import 'vm/machine.dart';
import 'libs/dart_api.dart';
import 'libs/lib.dart';
import 'libs/lib_std.dart';

export 'vm/i_machine.dart' show IMachine;
export 'libs/dart_api.dart' show DartCompoundApi, DartSimpleApi;
export 'libs/lib.dart' show Library, LoadLibrary;

class Interpreter {
  final Machine machine = Machine();

  Interpreter() {
    machine.loadLibrary(libStd);
  }

  List<dynamic> eval(String input) {
    final sentinal = Object();
    machine.pushObject(sentinal);

    machine.run(Parser.parse(Lexer.lex(input)));

    final List<dynamic> retval = [];
    while (true) {
      final next = machine.pop().value;
      if (next == sentinal) {
        return retval;
      }
      retval.insert(0, next);
    }
  }

  void bind(String name, dynamic value) => machine.setVar(name, value);
}
