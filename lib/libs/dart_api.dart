import 'package:flight_script/vm/command_base.dart';
import 'package:flight_script/vm/commands.dart';
import 'package:flight_script/vm/function.dart';
import 'package:flight_script/vm/i_machine.dart';
import 'package:flight_script/vm/value_base.dart';

/// Make all of the `call(Command)` style calls into methods on the machine
extension DartSimpleApi on IMachine {
  void push(FlightValue value) => call(Push(value));

  void pushBool(bool value) => call(PushBool(value));

  void pushNumber(double value) => call(PushNumber(value));

  void pushString(String value) => call(PushString(value));

  void pushDartFunction(DartFunction value) => call(PushDartFunction(value));

  void pushFunction(FlightFunction value) => call(PushFunction(value));

  void pushMap() => call(const PushMap());

  void pushObject(dynamic value) => call(PushObject(value));

  FlightValue pop() => call(const Pop());

  void store() => call(const Store());

  void load() => call(const Load());

  void execute() => call(const Execute());
}

/// useful compound methods
extension DartCompoundApi on IMachine {
  T popChecked<T>(FlightValueType type) {
    final value = pop();
    if (value.type != type) throw TypeError();
    return value.value;
  }

  bool popBool() => popChecked(FlightValueType.Bool);

  double popNumber() => popChecked(FlightValueType.Number);

  String popString() => popChecked(FlightValueType.String);

  dynamic popObject() => popChecked(FlightValueType.DartObject);

  Map<FlightValue, FlightValue> popMap() => throw "Not implemented";

  void pushDynamic(dynamic value) {
    if (value is num) {
      pushNumber(value.toDouble());
    } else if (value is bool) {
      pushBool(value);
    } else if (value is String) {
      pushString(value);
    } else if (value is DartFunction) {
      pushDartFunction(value);
    } else if (value is Map) {
      pushMap();
    } else {
      pushObject(value);
    }
  }

  // sets a named variable in the current frame
  void setVar(String name, dynamic value) {
    pushDynamic(value);
    pushString(name);
    store();
  }

  //gets a named variable from memory and pushes it onto the stack
  dynamic getVar(String name) {
    pushString(name);
    load();
  }

  void mapSet() {
    final value = pop();
    final key = pop();
    final map = pop();
    if (map.type != FlightValueType.Map) throw TypeError();
    map.value[key] = value;
    push(map);
  }

  void mapGet() {
    final key = pop();
    final map = pop();
    if (map.type != FlightValueType.Map) throw TypeError();
    final value = map.value[key];
    push(map);
    push(value);
  }

  void mapDelete() {
    final key = pop();
    final map = pop();
    if (map.type != FlightValueType.Map) throw TypeError();
    map.value.remove(key);
    push(map);
  }

  dynamic getAndExecute(String name){
    getVar(name);
    execute();
  }

  void run(Iterable<Command> program) {
    pushFunction(FlightFunction(program));
    execute();
  }

  void runDart(DartFunction main) {
    pushDartFunction(main);
    execute();
  }

  /// function call without constantly pushing/popping
  void fcall(FlightValue fn){
    push(fn);
    execute();
  }
}
