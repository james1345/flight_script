import 'package:flight_script/libs/lib.dart';
import 'package:flight_script/vm/i_machine.dart';
import 'package:flight_script/libs/dart_api.dart';

/// The standard library
/// It is possible to load the vm without this, but it's nearly useless
final libStd = Library({
  "true": true,
  "false": false,
  "dup": (IMachine state) => state.dup(),
  "!": (IMachine state) => state.pushBool(!state.popBool()),
  "+": (IMachine state) => state.pushNumber(
        state.popNumber() + state.popNumber(),
      ),
  "-": (IMachine state) =>
      state.pushNumber(-state.popNumber() + state.popNumber()),
  "*": (IMachine state) =>
      state.pushNumber(state.popNumber() * state.popNumber()),
  "/": (IMachine state) =>
      state.pushNumber((1.0 / state.popNumber()) * state.popNumber()),
  "=": (IMachine state) => state.pushBool(state.pop() == state.pop()),
  "!=": (IMachine state) => state.pushBool(state.pop() != state.pop()),
  ">": (IMachine state) =>
      state.pushBool(state.popNumber() < state.popNumber()),
  "<": (IMachine state) =>
      state.pushBool(state.popNumber() > state.popNumber()),
  ">=": (IMachine state) =>
      state.pushBool(state.popNumber() <= state.popNumber()),
  "<=": (IMachine state) =>
      state.pushBool(state.popNumber() >= state.popNumber()),
  "[]": (IMachine state) => state.pushMap(),
  "if": (IMachine state) => state.if_(),
  "ifelse": (IMachine state) => state.ifelse(),
  "loop": (IMachine state) => state.loop(),
  "mapSet": (IMachine state) => state.mapSet(),
  "mapGet": (IMachine state) => state.mapGet(),
  "print": (IMachine state) => print(state.pop().value),
  "inspect": (IMachine state) => print(state.pop()),
});

void _notImplemented(IMachine state) => throw "Not Implemented";

extension LibStd on IMachine {
  void dup() {
    final top = pop();
    push(top);
    push(top);
  }

  void if_() {
    final fn = pop();
    if (popBool()) {
      fcall(fn);
    }
  }

  void ifelse() {
    final elseFn = pop();
    final fn = pop();
    popBool() ? fcall(fn) : fcall(elseFn);
  }

  void loop() {
    final end = popNumber();
    final start = popNumber();
    final block = pop();
    for (var i = start; i <= end; i++) {
      pushNumber(i.toDouble());
      fcall(block);
    }
  }
}
