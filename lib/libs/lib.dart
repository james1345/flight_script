import 'package:flight_script/vm/i_machine.dart';
import 'package:flight_script/libs/dart_api.dart';

/// A library of dart functions to load into the global scope
///
/// The dart_api assumes nothing except `call` and commands are available
/// Libraries assume the dart_api is available, and add a load function
class Library {
  final Map<String, dynamic> words;

  const Library(this.words);
}

const _scopeSeparator = ".";

extension LoadLibrary on IMachine {
  void loadLibrary(Library library, {List<String> scope = const []}) {
    for (final binding in library.words.entries) {
      setVar(
        [...scope, binding.key].join(_scopeSeparator),
        binding.value,
      );
    }
  }
}
