import 'package:flight_script/rpn/token.dart';
import 'package:flight_script/vm/command_base.dart';
import 'package:flight_script/vm/commands.dart';
import 'package:flight_script/vm/function.dart';

abstract class Parser {
  static Iterable<Command> parse(Iterable<Token> tokens) =>
      _parse(tokens.iterator);

  static Iterable<Command> _parse(Iterator<Token> iterator) sync* {
    while (iterator.moveNext()) {
      final token = iterator.current;
      switch (token.type) {
        case TokenType.Number:
          yield PushNumber(double.parse(token.value));
          break;
        case TokenType.Symbol:
          yield PushString(token.value);
          yield const Load();
          break;
        case TokenType.Assignment:
          if (!iterator.moveNext() ||
              iterator.current.type != TokenType.Symbol) {
            throw StateError("Assignment not followed by valid symbol");
          }
          yield PushString(iterator.current.value);
          yield const Store();
          break;
        case TokenType.Execute:
          yield const Execute();
          break;
        case TokenType.FunctionStart:
          // We must call toList here, or iteration will continue in the wrong order
          yield PushFunction(
              FlightFunction(_parse(iterator).toList(growable: false)));
          break;
        case TokenType.FunctionEnd:
          return;
        case TokenType.String:
          yield PushString(token.value);
          break;
      }
    }
  }
}
