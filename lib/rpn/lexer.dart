import 'package:flight_script/rpn/token.dart';

extension on String {
  Iterable<String> asCharSequence() sync* {
    for (var i = 0; i < length; i++) {
      yield this[i];
    }
  }
}

const _whitespace = [" ", "\t", "\r", "\n"];
const _reserved = ["{", "}", ":", "(", ")"];

abstract class Lexer {
  static Iterable<Token> lex(String input) =>
      _lex(input.asCharSequence().iterator);

  static Iterable<Token> _lex(Iterator<String> iterator) sync* {
    while (iterator.moveNext()) {
      final char = iterator.current;
      if (_whitespace.contains(char)) {
        continue;
      } else if (char == "{") {
        yield const Token(TokenType.FunctionStart, "{");
      } else if (char == "}") {
        yield const Token(TokenType.FunctionEnd, "}");
      } else if (char == ":") {
        yield const Token(TokenType.Assignment, ":");
        if (!iterator.moveNext()) {
          throw StateError("Assignment not followed by symbol");
        }
        yield _lexSymbol(iterator);
      } else if (char == "(") {
        yield _lexExecute(iterator);
      } else if (char == "'" || char == '"') {
        yield _lexString(iterator);
      } else if (char == "-" || int.tryParse(char) != null) {
        // doubles start with a "-" or a "0-9"
        yield _lexNumber(iterator);
        // This method could give us a "-" symbol, so we have to check for execution
        if (iterator.current == "(") {
          yield _lexExecute(iterator);
        }
      } else {
        yield _lexSymbol(iterator);
        // after lexing a symbol we might have an immediate fcall, so we double check for that
        if (iterator.current == "(") {
          yield _lexExecute(iterator);
        }
      }
    }
  }

  static Token _lexExecute(Iterator<String> iterator) {
    if (!iterator.moveNext() || iterator.current != ")") {
      throw StateError("invalid syntax - ( not followed by )");
    }
    return const Token(TokenType.Execute, "()");
  }

  static Token _lexString(Iterator<String> iterator) {
    final builder = StringBuffer();
    final delimiter = iterator.current;
    while (iterator.moveNext()) {
      if (iterator.current == delimiter) {
        return Token(TokenType.String, builder.toString());
      }
      // use the \ to escape the next character and simply add it if it would otherwise be skipped
      if (iterator.current == "\\") {
        iterator.moveNext();
      }
      builder.write(iterator.current);
    }
    throw StateError("End of input reached while parsing string");
  }

  static Token _lexNumber(Iterator<String> iterator) {
    final builder = StringBuffer();
    builder.write(iterator.current);
    while (iterator.moveNext() &&
        !_whitespace.contains(iterator.current) &&
        !_reserved.contains(iterator.current)) {
      builder.write(iterator.current);
    }
    if (builder.toString() == "-") {
      // we have received a - on its own (probably as a word call)
      // return it as a symbol instead of a number
      return const Token(TokenType.Symbol, "-");
    }
    return Token(TokenType.Number, builder.toString());
  }

  static Token _lexSymbol(Iterator<String> iterator) {
    final builder = StringBuffer();
    builder.write(iterator.current);
    while (iterator.moveNext() &&
        !_whitespace.contains(iterator.current) &&
        !_reserved.contains(iterator.current)) {
      builder.write(iterator.current);
    }
    return Token(TokenType.Symbol, builder.toString());
  }
}
