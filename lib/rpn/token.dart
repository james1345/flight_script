enum TokenType {
  Number,
  Symbol,
  Assignment,
  Execute,
  FunctionStart,
  FunctionEnd,
  String,
}

class Token {
  final TokenType type;
  final String value;

  const Token(this.type, this.value);
}
